from Notebook.helpers import NoteBook


def notebook_ui(Notebook):

    while True:
        print("Welcome to progressive notebook:")
        print("1. Show all contacts")
        print("2. Add entry")
        print("3. Delete entry")
        print("4. Edit entry")
        print("5. Search")
        print("6. Sort by name")
        print("7. Sort by Surname")
        print("8. Exit")

        choice = input('Enter your choice: ')
        match choice:
            case '1':
                Notebook.show_all_contacts()
            case '2':
                Notebook.add_new_contact()
            case '3':
                Notebook.delete_contact()
            case '4':
                Notebook.update_contact()
            case '5':
                Notebook.find_by_mask()
            case '6':
                Notebook.sort_contacts_by_name()
            case '7':
                Notebook.sort_contacts_by_surname()
            case '8':
                Notebook.write_contacts()
                break


if __name__ == '__main__':
    a = NoteBook()
    notebook_ui(a)


