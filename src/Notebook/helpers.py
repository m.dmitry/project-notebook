from Notebook.models.contact import Contact
import csv
import re
import os


class SingletonMeta(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            instance = super().__call__(*args, **kwargs)
            cls._instances[cls] = instance
        return cls._instances[cls]


class RegistrationPattern:
    def __init__(self, name, pattern, error):
        self.name = name
        self.pattern = pattern
        self.error = error


class NoteBook(metaclass=SingletonMeta):
    def __init__(self):
        self.contact_inputs = self.get_contact_inputs()
        if os.path.isfile('data/contacts.csv'):
            self.contacts = self.get_contact_from_file()
            print(f'There are {len(self.contacts)} rows in file.')

    @staticmethod
    def get_contact_inputs():
        contact_inputs = [
            RegistrationPattern('Input name: ', r'^[a-zA-Z]+$', 'Invalid name. Please enter only alphabetical '
                                                                'characters.'),
            RegistrationPattern('Input surname: ', r'^[a-zA-Z]+$',
                                'Invalid surname. Please enter only alphabetical characters.'),
            RegistrationPattern('Input birth: ', r'^\d{2}\.\d{2}\.\d{4}$',
                                'Invalid date format. Please enter a date in the format dd.mm.yyyy.'),
            RegistrationPattern('Input address: ', r'^\d+\s[a-zA-Z]+\s[a-zA-Z]+$',
                                'Invalid address format. Please enter the address in the format "number street city.'),
            RegistrationPattern('Input number: ', r'^\d{3}-\d{3}-\d{4}$',
                                'Invalid phone number format, or this number in list. Please enter a phone number in '
                                'the format'
                                'xxx-xxx-xxxx.')
        ]
        return contact_inputs

    def show_all_contacts(self):
        for item in self.contacts:
            print(item)

    @staticmethod
    def get_contact_from_file():
        contacts = []
        with open('data/contacts.csv', 'rt') as f:
            reader = csv.reader(f, delimiter=',')
            next(reader)
            for line in reader:
                contacts.append(Contact(line[0], line[1], line[2], line[3], line[4]))
        return contacts

    @staticmethod
    def save_contact_in_file(contact):
        with open('data/contacts.csv', 'a', newline='') as f:
            writer = csv.writer(f)
            writer.writerow(contact.to_csv_row())

    @staticmethod
    def find_by_mask():
        mask = input("Input mask: ")
        with open('data/contacts.csv', 'r') as f:
            reader = csv.reader(f)
            next(reader)
            for row in reader:
                if row[1].lower().startswith(mask.lower()):
                    print(row)

    def add_new_contact(self):
        new_contact = self.create_new_contact()
        self.contacts.append(new_contact)
        print(f'Successful, add new contact: {new_contact}')

    def delete_contact(self):
        number = input('Enter your number: ')
        pattern = re.compile(r'^\d{3}-\d{3}-\d{4}$')
        if not pattern.match(number):
            return 'Invalid phone number format'

        contacts_to_keep = [contact for contact in self.contacts if contact.number != number]
        self.contacts = contacts_to_keep

        return f'Contact with number {number} has been successfully deleted'

    def update_contact(self):
        number = input('Enter the phone number of the contact you want to update (format xxx-xxx-xxxx): ')
        pattern = re.compile(r'^\d{3}-\d{3}-\d{4}$')
        if not pattern.match(number):
            return 'Invalid phone number format'

        for contact in self.contacts:
            if contact.number == number:
                new_contact = self.create_new_contact()
                contact.update(new_contact)
                return f'Contact with number {number} has been successfully updated'
        return f'Contact with number {number} not found'

    def sort_contacts_by_name(self):
        self.contacts = sorted(self.contacts, key=lambda contact: contact.name)
        return 'Contacts have been successfully sorted by name'

    def sort_contacts_by_surname(self):
        self.contacts = sorted(self.contacts, key=lambda contact: contact.surname)
        return 'Contacts have been successfully sorted by surname'

    def write_contacts(self):
        with open('data/contacts.csv', 'wt', newline='') as f:
            writer = csv.writer(f, delimiter=',')
            writer.writerow(['Name', 'Surname', 'Birthday', 'Address', 'Number'])
            for contact in self.contacts:
                writer.writerow(contact.to_csv_row())

    def create_new_contact(self):
        contact_fields = []
        for contact_input in self.contact_inputs:
            field_value = input(contact_input.name)
            while not re.match(contact_input.pattern, field_value):
                print(contact_input.error)
                field_value = input(contact_input.name)
            contact_fields.append(field_value)

        return Contact(contact_fields[0], contact_fields[1], contact_fields[2], contact_fields[3], contact_fields[4])
