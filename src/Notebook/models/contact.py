class Contact:
    def __init__(self, name, surname, birthday, address, number):
        self.name = name
        self.surname = surname
        self.birthday = birthday
        self.address = address
        self.number = number

    def __str__(self):
        return f"Contact(name={self.name}, surname='{self.surname}', birthday='{self.birthday}',\n\
                         address={self.address}, number={self.number})"

    def to_csv_row(self):
        return [str(self.name), str(self.surname), str(self.birthday), str(self.address), str(self.number)]

    def update(self, contact_from):
        self.name = contact_from.name
        self.surname = contact_from.surname
        self.birthday = contact_from.birthday
        self.address = contact_from.address
        self.number = contact_from.number
